$(function(){
  $(window).bind('scroll', winScroll).scroll();

  function winScroll() {
    var $win = $(window);
    if($win.scrollTop() > 30){
      // console.log('top show')
      $("header").addClass("scrolled");
    }else {
      $("header").removeClass("scrolled")
    }
  }
})