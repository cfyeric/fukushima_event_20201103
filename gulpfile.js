const { src, dest, parallel, series, watch } = require('gulp')
const pug = require('gulp-pug')
const sass = require('gulp-sass')
const uglifycss = require('gulp-uglifycss')
const autoprefixer = require('gulp-autoprefixer')
const babel = require('gulp-babel')
const eslint = require('gulp-eslint')
const uglify = require('gulp-uglify')
const plumber = require('gulp-plumber')
const imagemin = require('gulp-imagemin')
const replace = require('gulp-replace');
const browserSync = require('browser-sync').create()
const del = require('del')
const vinylPaths = require('vinyl-paths')

sass.compiler = require('node-sass')
const pugSetting = { basedir: 'src', pretty: true }

const cleanHtml = () => src(['build/**/*.html'], { read: false }).pipe(vinylPaths(del))
const renderPug = () =>
  src('src/html/pages/**/*.pug')
    .pipe(plumber())
    .pipe(pug(pugSetting))
    .pipe(dest('build'))
    .pipe(browserSync.stream())

// const watchHtml = () => watch(['src/html/pages/**/*.pug', 'src/html/templates/**/*.pug', 'src/html/include/**/*.pug'], html)
const watchHtml = () => watch(['src/html/**/*.pug'], html)

const html = series(cleanHtml, renderPug)

const cleanCss = () => src('build/assets/styles/**/*.css','src/styles/amp/*.css', { read: false, allowEmpty: true }).pipe(vinylPaths(del))

const renderSass = () =>
  src(['src/styles/*.scss', 'src/styles/pages/*.scss'])
    .pipe(plumber())
    .pipe(sass().on('error', sass.logError))
    .pipe(
      autoprefixer({
        overrideBrowserslist: ['last 5 versions', 'ie 9-11'],
        cascade: false
      })
    )
    .pipe(uglifycss())
    // .pipe(gulpif(production, uglifycss()))
    .pipe(dest('build/assets/styles'))
    .pipe(browserSync.stream())

// const renderLibSass = () =>
//   src(['src/styles/lib/**/*.scss'])
//     .pipe(plumber())
//     .pipe(sass().on('error', sass.logError))
//     .pipe(
//       autoprefixer({
//         overrideBrowserslist: ['last 5 versions', 'ie 9-11'],
//         cascade: false
//       })
//     )
//     .pipe(uglifycss())
//     // .pipe(gulpif(production, uglifycss()))
//     .pipe(dest('build/assets/styles/lib'))
//     .pipe(browserSync.stream())

// const renderAmpSass = () =>
//   src(['src/styles/amp/*.scss'])
//     .pipe(plumber())
//     .pipe(sass().on('error', sass.logError))
//     .pipe(
//       autoprefixer({
//         overrideBrowserslist: ['last 5 versions', 'ie 9-11'],
//         cascade: false
//       })
//     )
//     .pipe(uglifycss())
//     .pipe(replace('@charset "UTF-8";', ''))
//     .pipe(dest('src/styles/amp/'))
//     .pipe(browserSync.stream())

// const watchCss = () => watch(['src/styles/**/*.scss', '!src/styles/bootstrap-custom.scss'], css)
const watchCss = () => watch(['src/styles/**/*.scss'], css)

const css = series(cleanCss, renderSass)

const cleanJs = () => src('build/assets/scripts/', { read: false, allowEmpty: true }).pipe(vinylPaths(del))

const renderJs = () =>
  src('src/scripts/*.js')
    .pipe(plumber())
    .pipe(eslint())
    .pipe(eslint.format())
    // .pipe(eslint.failAfterError())
    .pipe(
      babel({
        presets: ['@babel/env']
      })
    )
    .pipe(uglify())
    .pipe(dest('build/assets/scripts'))
    .pipe(browserSync.stream())

const moveLibJs = () => src('src/scripts/lib/**/*').pipe(dest('build/assets/scripts/lib'))

const watchJs = () => watch('src/scripts/**/*', js)

const js = series(cleanJs, renderJs, moveLibJs)

const cleanImage = () => src('build/assets/images/', { read: false, allowEmpty: true }).pipe(vinylPaths(del))

const moveImage = () =>
  src('src/images/**/*')
    .pipe(imagemin())
    .pipe(dest('build/assets/images'))

const image = series(cleanImage, moveImage)

const watchImage = () => watch('src/images/**/*', image)

const devServer = () => {
  browserSync.init({
    server: {
      baseDir: 'build/',
      directory: true
    },
    port: 8080,
    host: '0.0.0.0',
    ui: false
  })
}

const renderAll = parallel(html, css, js, image)
const watchAll = parallel(watchHtml, watchCss, watchJs, watchImage)
// const watchAll = parallel(watchHtml, watchJs, watchImage)
const start = series(renderAll, parallel(watchAll, devServer))

exports.build = renderAll
exports.default = start

exports.image = image
exports.watchImage = watchImage
